<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title></title>
  <!-- Plugins CSS -->
  <link rel="stylesheet" href="./plugins/bootstrap-4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="./plugins/meanmenu/meanmenu.css">
  <link rel="stylesheet" href="./plugins/slick-1.8.1/slick.css">
  <link rel="stylesheet" href="./plugins/fancybox-master/jquery.fancybox.min.css">
  <link rel="stylesheet" href="./plugins/aos-animation/aos.css">
  <!-- fonts -->
  <link rel="stylesheet" href="./fonts/ep-icon-fonts/css/style.css">
  <link rel="stylesheet" href="./fonts/fontawesome-5/css/all.min.css">
  <!-- Custom Stylesheet -->
  <link rel="stylesheet" href="./css/settings.css">
  <link rel="stylesheet" href="./css/style.css">
</head>

<body >
<div class="site-wrapper">
<!-- Header Starts -->
  <header class="site-header position-relative">
        <div class="container">
            <div class="row justify-content-center align-items-center position-relative">
                <div class="col-sm-3 col-6 col-lg-2 col-xl-2 order-lg-1">
                  <!-- Brand Logo -->
                    <div class="brand-logo">
                        <a href=""><img src="image/main-logo.png" alt="" /></a>
                    </div>
                </div>
               
            </div>
        </div>
        <div class="shape-holder header-shape" data-aos="fade-down" data-aos-once="true"><img src="image/header-shape.svg" alt=""></div>
  </header>

<!-- Hero-Area -->
<div class="hero-area position-relative">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-7 col-12">
        <div class="hero-content">
          <h2>Pelatihan Peluang Usaha</h2>
          <h2>Baby & Mom Spa</h2>
          <p>Bergabunglah bersama kami bagi Anda yang berminat mencari profit dibisnis kesehatan dan membuka layanan usaha baby spa & mom treatment. Terbuka untuk dokter, bidan, perawat, tenaga kesehatan lainnya, dan umum.</p>
          <!-- <p>Insed Education atau Insed Edu adalah lembaga pendidikan non formal yang didirikan sebagai bagian dari upaya untuk ikut serta mencerdaskan kehidupan bangsa. Kegiatan Pelatihan yang kami kembangkan ini bertujuan untuk membuka usaha.</p> -->
          <div class="hero-btn"><a href="#" class="btn btn--orange btn--hover-shine"  data-toggle="modal" data-target="#exampleModal">Daftar Sekarang</a></div>
          <div class="rating">
            <div class="icon">
              <i class="fas fa-star active"></i>
              <i class="fas fa-star active"></i>
              <i class="fas fa-star active"></i>
              <i class="fas fa-star active"></i>
              <i class="fas fa-star active"></i>
            </div>
            <span class="text">Sudah meluluskan lebih dari 500 orang</span>
          </div>
        </div>
      </div>

      <div class="col-xl-6 col-lg-6 col-md-5 col-12">
        <div class="hero-media">
          <img src="image/hero.jpg" alt="">
          <a class="video-play-trigger" data-fancybox href="https://www.youtube.com/watch?v=J1bWEZQZCvA "><i class="icon icon-triangle-right-17-2"></i></a>
          <div class="media-shape animate-spin"><img src="image/media-shape.png" alt=""></div>
        </div>
      </div>
    </div>
  </div>
  <div class="shape-holder hero-shape" data-aos="fade-right" data-aos-once="true"><img src="image/hero-orange-shape.svg" alt=""></div>
</div>

<!-- Facts Section -->
<section class="facts-section">
  <div class="container">
    <div class="position-relative">
      <div class="fact-absolute">
          <div class="row justify-content-center">
              <!-- <div class="col-xl-7 col-lg-8 mb--35  text-center">
                <span class="fact-text">The #1 User Experience Design course in the market</span>
              </div> -->
            </div>
            <div class="row justify-content-center space-db--30">
                <div class="col-md-6 col-lg-3 mb--30" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                        <div class="fact-card" >
                          <div class="card-content">
                            <span class="number">500 +</span>
                            <p class="info">Lulusan</p>
                          </div>
                
                          <div class="card-icon">
                            <img src="image/users-wm.png" alt="">
                          </div>
                        </div>
                </div>
                
                <div class="col-md-6 col-lg-3 mb--30" data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="200">
                        <div class="fact-card" >
                          <div class="card-content">
                            <span class="number">4</span>
                            <p class="info">Trainer Bersertifikasi BNSP</p>
                          </div>
                
                          <div class="card-icon">
                            <img src="image/fact-2.png" alt="">
                          </div>
                        </div>
                </div>
                
                <!-- <div class="col-md-6 col-lg-3 mb--30" data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="400">
                        <div class="fact-card" >
                          <div class="card-content">
                            <span class="number"></span>
                            <p class="info">Peluang Usaha</p>
                          </div>
                
                          <div class="card-icon">
                            <img src="image/fact-3.png" alt="">
                          </div>
                        </div>
                </div>
                
                <div class="col-md-6 col-lg-3 mb--30" data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="600">
                        <div class="fact-card" >
                          <div class="card-content">
                            <span class="number">21k</span>
                            <p class="info">pelanggan puas</p>
                          </div>
                
                          <div class="card-icon">
                            <img src="image/fact-4.png" alt="">
                          </div>
                        </div>
                </div> -->
            </div>
      </div>
    </div>
  </div>
</section>


<!-- Course section -->
<section class="course-section position-relative">
  <div class="shape-holder">
    <img src="./image/section-2-shape-bg.svg" alt="">
  </div>
  <div class="shape-holder" data-aos="zoom-in"  data-aos-once="true">
    <img src="./image/section-2-shape.svg" alt="">
  </div>
  <div class="shape-holder course-shape-3" data-aos="zoom-in-left" data-aos-once="true">
    <img src="./image/course-yelloow-svg.svg" alt="">
  </div>
  <div class="container pt-lg--85">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <div class="section-title section-black">
          <h2>Fasilitas yang didapatkan</h2>
          <p>Demi menunjang kegiatan, maka para peserta pelatihan berhak mendapatkan berbagai fasilitas menarik dari kami diantaranya sebagai berikut.</p>
        </div>
      </div>
    </div>
    <div class="accordion course-accordion" id="accordionExample" >
      <div class="card course-card"  data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
        <div class="card-header" id="headingOne">
          <div class="row">
              <div class="col-xl-6 col-lg-7">
                <div class="acc-left-content d-flex">
                  <div class="acc-icon">
                    <img  src="./image/coffee-break.svg" alt="">
                  </div>
                  <div class="acc-head-contents">
                    <h3>2x Lunch 4x Coffee Break</h3>
                    <p>2 Hari pelatihan dihotel berbintang dengan tempat yang nyaman, strategis, dan menu makanan yang bervariasi.</p>
                  </div>
                </div>
              </div>
              
          </div>
          
        </div>
    
       
      </div>
      <div class="card course-card"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="200">
        <div class="card-header" id="heading2">
          <div class="row">
              <div class="col-lg-6">
                <div class="acc-left-content d-flex">
                  <div class="acc-icon">
                    <img src="./image/sertifikat.svg" alt="">
                  </div>
                  <div class="acc-head-contents">
                    <h3>Sertifikat Lembaga Resmi</h3>
                    <p>Dapatkan Sertifikat lembaga resmi dari kami yang bisa di gunakan untuk membuka jasa usaha.</p>
                  </div>
                </div>
              </div>
          </div>
          
        </div>
      </div>
      <div class="card course-card"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="400">
        <div class="card-header" id="heading3">
          <div class="row">
              <div class="col-lg-6">
                <div class="acc-left-content d-flex">
                  <div class="acc-icon">
                    <img src="./image/video.svg" style="width: 80%;" alt="">
                  </div>
                  <div class="acc-head-contents">
                    <h3>Video Panduan Praktik senilai 1 juta</h3>
                    <p>Gratis video materi yang bisa Anda lihat kapan dan dimanapun.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 offset-lg-2">
                <div class="acc-right-content d-flex">
                  <div class="course-length-badge">
                    <span>12 Video Praktik</span>
                    <span>60 menit</span>
                  </div>
                </div>
              </div>
          </div>
          
        </div>
    
      </div>
      <div class="card course-card"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="600">
        <div class="card-header" id="heading4">
          <div class="row">
              <div class="col-lg-6">
                <div class="acc-left-content d-flex">
                  <div class="acc-icon">
                    <img src="./image/seminarkit.svg" alt="">
                  </div>
                  <div class="acc-head-contents">
                    <h3>Seminar Kit</h3> 
                    <p>Gratis peralatan untuk mengikuti pelatihan seperti Goody bag, Notebook, Pulpen, Minyak untuk praktik, Bandana, Mangkok + kuas set.</p>
                  </div>
                </div>
              </div>
              </div>
          
        </div>
    
      </div>

      <div class="card course-card"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="600">
        <div class="card-header" id="heading4">
          <div class="row">
              <div class="col-lg-6">
                <div class="acc-left-content d-flex">
                  <div class="acc-icon">
                    <img src="./image/rapid.svg" alt="">
                  </div>
                  <div class="acc-head-contents">
                    <h3>Rapid Test</h3> 
                    <p>Kami memastikan menjalankan protokol kesehatan di masa pandemi dengan memberikan Rapid Test Serologi dan Rapid Test Antigen sebelum acara pelatihan dimulai.</p>
                  </div>
                </div>
              </div>
              </div>
        </div>
      </div>

      <div class="card course-card"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="600">
        <div class="card-header" id="heading4">
          <div class="row">
              <div class="col-lg-6">
                <div class="acc-left-content d-flex">
                  <div class="acc-icon">
                    <img src="./image/bimbingan.svg" alt="">
                  </div>
                  <div class="acc-head-contents">
                    <h3>Bimbingan Bussines Plan </h3> 
                    <p>Sebagai bentuk dukungan dan tanggung jawab. Kami memberikan bimbingan berupa konsultasi gratis seumur hidup dan dukungan promosi digital lewat jaringan kami yang tersebar di seluruh indonesia</p>
                  </div>
                </div>
              </div>
              </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Aurthor Starts -->
<div class="aurthor-section">
  <div class="container">
    <div class="section-title section-black">
      <h2>Trainer</h2>
    </div>
    <div class="row  spacing-bottom mb--10">
      <div class="col-sm-6">
          <div class="row">
            <div class=" col-lg-5 col-sm-4" data-aos="zoom-in-right"  data-aos-once="true" data-aos-delay="55" data-aos-duration="1500">
              <div class="aurthor-image">
                <img src="image/IMG-20201129-WA0029.jpg" alt="">
              </div>
            </div>

            <div class="col-lg-6 col-sm-6" data-aos="zoom-in-left"  data-aos-once="true" data-aos-delay="55" data-aos-duration="1500">
              <div class="aurthor-content" style="margin-top:2%;">
                <h4>Arief Rahman Hakim.</h4>
                <p style="font-size:16px; padding-right: 0px;"> Berpengalaman lebih dari 15 tahun dibidang tumbuh kembang anak, pemilik dari klinik sultan baby spa & mom care dan telah TOT BNSP
                </p>
                <p style="font-size:16px; padding-right: 0px;">
                <h6>Materi Baby Spa</h6>
                  <ul style="font-size:16px; padding-right: 0px;">
                    <li>Baby Massage</li>
                    <li>Baby Swim</li>
                    <li>Baby Gym</li>
                    <li>Pengantar Tumbuh Kembang</li>
                    <li>Premature Infant Massage & BBLR</li>
                    <li>Brain Gym For Baby</li>
                    <li>Gym Ball Exercise Stimulation for Baby</li>
                    <li>Terapi Massage Untuk Bayi Kondisi
                        <ol style="margin-left:30px;"> 
                          <li>Colic ( Kembung )</li>
                          <li>Konstipasi</li>
                          <li>Susah BAB</li>
                          <li>Diare</li>
                          <li>Pijat Nafsu Makan</li>
                          <li>Batuk Pilek</li>
                        </ol>
                    </li>
                  </ul>
                </p>
              </div>
            </div>
          </div>
      </div>

      <div class="col-sm-6">
        <div class="row">
          <div class=" col-lg-5 col-sm-4" data-aos="zoom-in-right"  data-aos-once="true" data-aos-delay="55" data-aos-duration="1500">
            <div class="aurthor-image">
              <img src="image/822f736b-5962-48a1-9b4b-a96242524c3c.jpeg" alt="">
            </div>
          </div>

          <div class="col-lg-6 col-sm-6" data-aos="zoom-in-left"  data-aos-once="true" data-aos-delay="55" data-aos-duration="1500">
            <div class="aurthor-content" style="margin-top:2%;">
              <h4>Shovilia Krisnawan.</h4>
              <p style="font-size:16px; padding-right: 0px;"> Dosen Terbaik Piagam Direktur Akademi Keperawatan Hang Tuah,
                meraih penghargaanThe Best Employee Erha Clinic Indonesia 2012 dan telah TOT BNSP
              </p>
              <p>
              <h6>Materi Mom Spa</h6>
                <ul style="font-size:16px; padding-right: 0px;">
                  <li>Pregnant & Post Natal Relaxasion Massage</li>
                  <li>Ocitoxin & Lactation Massage</li>
                  <li>Sliming Moksibasi Accupresure</li>
                  <li>ToVag Accupresure</li>
                  <li>Nature Beauty Spa</li>
                  <li>Foot Spa & Bengkung</li>
                  <li>Aplikasi Cleansing Masker</li>
                  <li>Bussines Plan Baby & Mom Spa<li>
                </ul>
              </p>
            </div>
          </div>
        </div>
    </div>

      
    </div>
    <span class="section-devider"></span>
  </div>
</div>

<!-- Testimonial Starts -->
<!-- <section class="testimonial-section position-relative">
  <div class="container">
      <div class="row">
          <div class="col-lg-7 col-xl-6">
              <h2 class="">Don’t believe me. Listen what’s my students are</h2>
          </div>
      </div>
      <div class="testimonial-slider-wrapper">
          <div class="slider-btns"></div>
          <div class="testimonial-slider">
              <div class="single-slide" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                  <div class="testimonial-card">
                      <div class="image">
                          <img src="./image/t4-card-2.jpg" alt="">
                      </div>
                      <div class="content">
                          <h4>Annie Reyes</h4>
                          <span class="d-block">Founder of Crips</span>
                          <a class="video-play-trigger" data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk"><i class="icon icon-triangle-right-17-2"></i></a>   
                      </div>
                  </div>
              </div>
              <div class="single-slide" data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="200">
                  <div class="testimonial-card">
                      <div class="image">
                          <img src="./image/t4-card-3.jpg" alt="">
                      </div>
                      <div class="content">
                          <h4>Jay Fletcher</h4>
                          <span class="d-block">Marketing Head at Gigri Solution</span>
                          <a class="video-play-trigger" data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk"><i class="icon icon-triangle-right-17-2"></i></a>   
                      </div>
                  </div>
              </div>
              <div class="single-slide"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="400">
                  <div class="testimonial-card">
                      <div class="image">
                          <img src="./image/t4-card-1.jpg" alt="">
                      </div>
                      <div class="content">
                          <h4>Arthur Keller</h4>
                          <span class="d-block">CTO at Drivin</span>
                          <a class="video-play-trigger" data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk"><i class="icon icon-triangle-right-17-2"></i></a>   
                      </div>
                  </div>
              </div>
              <div class="single-slide"  data-aos="fade-up" data-aos-duration="2000" data-aos-once="true" data-aos-delay="600">
                  <div class="testimonial-card">
                      <div class="image">
                          <img src="./image/t4-card-1.jpg" alt="">
                      </div>
                      <div class="content">
                          <h4>Annie Reyes</h4>
                          <span class="d-block">Founder of Crips</span>
                          <a class="video-play-trigger" data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk"><i class="icon icon-triangle-right-17-2"></i></a>      
                      </div>
                  </div>
              </div>
          </div>
      
      </div>
  </div>
  <div class="shape-holder testimonial-shape" data-aos="zoom-in-left" data-aos-once="true"><img src="image/testimonial-shape.svg" alt=""></div>
</section> -->


<!-- FAQ Section -->
<section class="faq-section position-relative">
  <div class="container">
  <!-- faq accordionExample -->
  <div class="faq-accordion">
    <div class="row justify-content-center">
      <div class="col-lg-10">
        <div class="section-title"><h2>Frequently Asked Questions</h2></div>
  
        <div class="accordion faq-accordion space-db--10" id="accordionExample2">
          <div class="single-faq mb--10" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true" data-aos-delay="200" data-aos-once="true">
                <button class="faq-head collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">
                  <h2>Apakah ada garansi uang kembali?</h2>
                  <i class="icon icon-minimal-down"></i>
                </button>
        
            <div id="faq2" class="collapse" aria-labelledby="faq-heading2" data-parent="#accordionExample2">
              <div class="faq-body">
                <p>
                  Jika terjadi pembatalan dari pihak penyelenggara maka biaya akan di kembalikan 100%.
                </p>
              </div>
              
            </div>
          </div>
          <div class="single-faq mb--10" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true" data-aos-delay="400">
                <button class="faq-head collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">
                  <h2>Bagaimana saya mengetahui jadwal pelatihan di kota saya?</h2>
                  <i class="icon icon-minimal-down"></i>
                </button>
        
            <div id="faq3" class="collapse" aria-labelledby="faq-heading3" data-parent="#accordionExample2">
              <div class="faq-body">
              <p>
                Bila Anda sudah tergabung dalam jaringan kami, Anda dapat mengetahui melalui nomor call center official atau website resmi Insededu atau melalui webinar yang kami selenggarakan.
               </p>
              </div>
              
            </div>
          </div>
          <div class="single-faq mb--10" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true" data-aos-delay="600">
                <button class="faq-head collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">
                  <h2>Bagaimana jika hasil Rapid Test saya tidak lolos?</h2>
                  <i class="icon icon-minimal-down"></i>
                </button>
        
            <div id="faq4" class="collapse" aria-labelledby="faq-heading4" data-parent="#accordionExample2">
              <div class="faq-body">
                <p>Peserta yang dinyatakan reaktif ketika rapid test, maka akan dikembalikan 100% biaya pelatihan yang telah dibayarkan.</p>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="faq-guarantee">
    <div class="row">
      <div class="col-lg-5 col-md-7 offset-lg-1" data-aos="zoom-in-right"  data-aos-once="true" data-aos-delay="55" data-aos-duration="1500">
        <div class="content">
          <h2><span class="text--primary fs-inherit">Dapatkan</span> Penginapan gratis</h2>
          <p>Dapatkan penginapan gratis untuk setiap pendaftar tercepat. Untuk informasi lebih lanjut hubungi contact person kami. <a href="tel:+6281369395745">(081369395745)</a></p>
        </div>
      </div>

      <div class="col-lg-4 col-md-5 offset-lg-1" data-aos="zoom-in-left"  data-aos-once="true" data-aos-delay="55" data-aos-duration="1500">
        <div class="flex-all-center h-100">
          <img src="image/guarnatee.png" alt="">
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="shape-holder faq-shape" data-aos="zoom-in-right" data-aos-once="true"><img src="image/faq-shape.svg" alt=""></div>
</section>



<!-- Footer Section -->
<div class="footer-section position-relative" style="text-align: center; padding: 0px;">
  <div class="container">
    
      <div class="row">
          <div class="col-sm-12 ">
        
                <p style="padding-top: 2%;">© Copyright 2020. All Rights Reserved.
                   Powered by Insededu
                  </p>
              </div>
          </div>
        </div>
  </div>
 </div>
</div>
</div>

  <div class="modal fade" tabindex="-1" id="exampleModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Formulir Pendaftaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <?php
          $servername = "localhost";
          $username = "insm1326_wp966";
          $password = "9k-S33pS)u";
          $dbname = "insm1326_wp966";

          // Create connection
          $conn = mysqli_connect($servername, $username, $password, $dbname);

          // Check connection
          if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
          }
        ?>
        <?php if($_POST){ 
			    $sql = "SELECT email FROM event WHERE email='".$_POST['email']."'";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                     echo "<h2>Mohon maaf, email sudah terdaftar</h2>";
                     exit;
                }
                
			    $sql = "INSERT INTO event (nama, email, pekerjaan,wa,instansi,alamat,fb,ig) VALUES (
			            '".$_POST['nama']."',
                        '".$_POST['email']."', 
                        '".$_POST['pekerjaan']."',
                        '".$_POST['wa']."',
                        '".$_POST['instansi']."',
                        '".$_POST['alamat']."',
                        '".$_POST['fb']."',
                        '".$_POST['ig']."'
			             )";
                    			   if (mysqli_query($conn, $sql)) {
                      echo "<h2>Terima kasih telah mendaftar</h2>
                      <p>Data Anda telah barhasil tersimpan.</p>";
                    } else {
                      echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                    }
                exit;
            }else{?>
          <form method="post">
						<div class="form-group"> 
							<label class="header">Nama</label>
							<input type="text" id="name" name="nama"  required=""  class="form-control">
						</div>
					
						<div class="form-group">	
							<label class="header">Email <span>:</span></label>
							<input type="email" id="email" name="email"  required="" class="form-control">
						</div>
					
						<div  class="form-group">	
							<label class="header">No Whatsapp <span>:</span></label>	
							<input type="text" id="usrtel" name="wa" required="" class="form-control">
						</div>	
						<div  class="form-group">	
							<label class="header">Instansi <span>:</span></label>	
							<input type="text" id="usrtel" name="instansi" class="form-control">
						</div>	
						<div  class="form-group">
								<label class="header">Pekerjaan</label>
								<select name="pekerjaan" required class="form-control">
									
									<option value="Dokter">Dokter </option>
									<option value="Perawat">Perawat</option>
									<option value="Bidan">Bidan</option>
									<option value="Mahasiswa Kesehatan">Mahasiswa Kesehatan</option>
									<option value="PNS">PNS</option>
									<option value="Umum">Umum</option>
								</select>
						</div>
					

						<div  class="form-group">
							<label class="enquiry">Alamat Lengkap <span>:</span></label>
							<textarea id="message" name="alamat" class="form-control"></textarea>
							<div class="clear"></div>
						</div>
						
						<div  class="form-group">	
							<label class="header">Facebook ID <span>:</span></label>	
							<input type="text" id="usrtel" name="fb" class="form-control">
						</div>	
						
						<div  class="form-group">	
							<label class="header">Instagram ID<span>:</span></label>	
							<input type="text" id="usrtel" name="ig" class="form-control">
						</div>	
						
							<input type="submit" value="Daftar" class="btn btn-primary">
          </form>
            <?php }?>
        </div>
      </div>
    </div>
  </div>
  <!-- Vendor JS-->
  <script src="./plugins/jquery/jquery.min.js"></script>
  <script src="./plugins/jquery/jquery-migrate.min.js"></script>
  <script src="./plugins/bootstrap-4.3.1/js/bootstrap.bundle.js"></script>

  <!-- Plugins JS -->
  <script src="./plugins/meanmenu/jquery.meanmenu.js"></script>
  <script src="./plugins/slick-1.8.1/slick.min.js"></script>
  <script src="./plugins/fancybox-master/jquery.fancybox.min.js"></script>
  <script src="./plugins/aos-animation/aos.js"></script>

  <!-- Custom JS -->
  <script src="./js/active.js"></script>
</body>

</html>
